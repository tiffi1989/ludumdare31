﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour {

    public Text text;
    private Slider slider;
    public GameObject player;
    private int maxHealth;
    private CharController charCont;

   
	// Use this for initialization
	void Start () {
        slider = gameObject.GetComponent<Slider>();
       charCont = player.GetComponent<CharController>();
       maxHealth = charCont.maxHealth;
	}
	
	// Update is called once per frame
	void Update () {

        maxHealth = charCont.maxHealth;

        text.text = slider.value * maxHealth + " / "+ maxHealth;
	}
}
