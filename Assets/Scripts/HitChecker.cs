﻿using UnityEngine;
using System.Collections;

public class HitChecker : MonoBehaviour {

    private int health;
    private bool visible;
    public int speed;
    public ParticleSystem particle;
    private GameObject spawnPoint;
    private GameObject player;
    public GameObject bubble;
    private GameObject canvas;
    private float time;
    private float timehit;
    private Animator animator;
    private bool dead;
    private int damage;


	// Use this for initialization
	void Start () {
        visible = false;
        transform.eulerAngles = new Vector3(0, 0, 180);
        spawnPoint = GameObject.FindGameObjectWithTag("SpawnPoint");
        Transform[] allChildren = spawnPoint.GetComponentsInChildren<Transform>();
        allChildren[1].transform.parent = gameObject.transform;
        particle = gameObject.GetComponentInChildren<ParticleSystem>();
        player = GameObject.FindGameObjectWithTag("PlayerTag");
        canvas = GameObject.FindGameObjectWithTag("Canvas");
        animator = gameObject.GetComponent<Animator>();
        CharController character = player.GetComponent<CharController>();
        damage = character.damage;
	
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        timehit += Time.deltaTime;

        if (!dead)
        {
            if (timehit >= Random.Range(2f, 5f))
            {
                animator.SetTrigger("fight");
                timehit = 0;
            }


            if (health <= 0)
            {
                dead = true;
                Death();
            }

            if (visible)
            {

                Vector3 otherVec = player.transform.position;
                Vector3 thisVec = gameObject.transform.position;

                float angle = Mathf.Atan2(otherVec.y - thisVec.y, otherVec.x - thisVec.x) * Mathf.Rad2Deg;




                gameObject.transform.eulerAngles = new Vector3(0, 0, angle);
                transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime * speed);
            }
            else
            {

                transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime * speed);
                transform.eulerAngles = new Vector3(0, 0, 90);
            }
        }
	}

    void Death()
    {
    
        particle.transform.parent = spawnPoint.transform ;

        gameObject.GetComponent<Animator>().SetTrigger("dead");

        

        Invoke("Kill", 2);
    }

    void Kill()
    {
        Destroy(gameObject);
    }


    void OnBecameVisible()
    {
        Invoke("setVisibleTrue", 1f);
    }

    void setVisibleTrue()
    {
        visible = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Sword") && time > 0.5f)
        {
            time = 0;
            health -= damage;

            if (!dead)
            {
                GameObject theBubble = Instantiate(bubble, new Vector3(0, 30, 0), Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
                theBubble.transform.SetParent(canvas.transform);

                float myWidth = 784f;
                float screenWidth = Screen.width;

                float ratio = screenWidth / myWidth;


                theBubble.transform.localScale = new Vector3(1, 1, 1);
                theBubble.GetComponent<RectTransform>().pivot = new Vector2(-(transform.position.x / 2f * ratio - 0.5f), -(transform.position.y / 2f * ratio - 0.5f));


                RectTransform rectTrans = theBubble.GetComponent<RectTransform>();
                rectTrans.transform.position = new Vector3(rectTrans.transform.position.x, rectTrans.transform.position.y, damage);

            }


            Vector3 otherVec = other.gameObject.transform.position;
            Vector3 thisVec = gameObject.transform.position;

            float angle = Mathf.Atan2(otherVec.y - thisVec.y, otherVec.x - thisVec.x) * Mathf.Rad2Deg;


            particle.gameObject.transform.eulerAngles = new Vector3(angle, 270, 180);

            particle.Play();
            GetComponent<AudioSource>().Play();

        }
        
    }

    public void SetLife(int life)
    {
        health = life;
    }
}
