﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharController : MonoBehaviour {

    public int runSpeed;
    private Animator animator;
    public float health;
    private int armor;
    public int damage;
    public Slider healthbar;
    private bool startEnded;
    private bool start;
    public GameObject mobSpawner;
    public Canvas canvas;
    private float time;
    float shakeAmt = 0;
    private int enemyDamage;
    public ParticleSystem particle2;
    public int maxHealth;

    public Camera mainCamera;
    private bool justOneDead;
    

	// Use this for initialization
	void Start () {
        animator = gameObject.GetComponent<Animator>();
        startEnded = false;
        damage = 15;
        enemyDamage = 10;
        maxHealth = 100;
	
	}
	
	// Update is called once per frame
	void Update () {
        if (start && startEnded && health >0)
        {
            MoveControl();
            EnforceBounds();
        }
        
        if(start && !startEnded)
        {
            MoveStart();
        }
        healthbar.value = health / maxHealth;

        if (health <= 0 && !justOneDead)
        {
            Invoke("Die", 2);
            animator.SetTrigger("dead");
            particle2.Play();
            justOneDead = true;
            
        }

        time += Time.deltaTime;
	}

    void MoveStart()
    {
        transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime * runSpeed);
        transform.eulerAngles = new Vector3(0, 0, 90);
    }

    void OnBecameVisible()
    {
        Invoke("setStartEnded", 0.7f);
    }

    void Die()
    {
        canvas.GetComponent<Animator>().SetBool("dead", true);
    }




    void MoveControl()
    {
        float translationX = Input.GetAxis("Horizontal") * runSpeed;
        float translationY = Input.GetAxis("Vertical") * runSpeed;

        if (translationX != 0 || translationY != 0)
        {
            animator.SetBool("isWalking", true);
            translationX *= Time.deltaTime;
            translationY *= Time.deltaTime;

            Vector3 moveDirection = new Vector3(translationX, -translationY, 0);

            float angle = Mathf.Atan2(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal")) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
           // moveDirection = Quaternion.AngleAxis(angle, Vector3.forward) * moveDirection;
            moveDirection = Quaternion.AngleAxis(angle, Vector3.forward) * moveDirection;
            transform.Translate(moveDirection);

        }
        else
        {
            animator.SetBool("isWalking", false);
        }

        if ((Input.GetKey("[1]") || Input.GetKey("space") || Input.GetKey("m")) && !animator.GetCurrentAnimatorStateInfo(0).IsName("SwordSwing"))
        {
            animator.SetTrigger("fight");
        }

         
            
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "EnemySword")
        {
            
            if (health > 0 && time > 0.5f)
            {
                health -= enemyDamage;

                time = 0;
            }

            shakeAmt = 25 * .0025f;
            InvokeRepeating("CameraShake", 0, .01f);
            Invoke("StopShaking", 0.3f);
            GetComponent<AudioSource>().Play();
        }

        if (other.gameObject.tag == "Bomb")
        {
            if (health > 0 && time > 0.5f)
            {
                health -= 50;

                time = 0;
            }

            GetComponent<AudioSource>().Play();



        }
    }

    public void shakeIt()
    {
        shakeAmt = 25 * .0025f;
        InvokeRepeating("CameraShake", 0, .01f);
        Invoke("StopShaking", 0.3f);
    }

    public void SetEnemyDamage(int dmg)
    {
        enemyDamage = dmg;
    }

    private void EnforceBounds()
    {
        Vector3 newPosition = transform.position;
        Camera mainCamera = Camera.main;
        Vector3 cameraPosition = mainCamera.transform.position;

        float xDist = mainCamera.aspect * mainCamera.orthographicSize;
        float xMax = cameraPosition.x + xDist;
        float xMin = cameraPosition.x - xDist;

        if (newPosition.x < xMin || newPosition.x > xMax)
        {
            newPosition.x = Mathf.Clamp(newPosition.x, xMin, xMax);
        }

        float yMax = mainCamera.orthographicSize;

        if (newPosition.y < -yMax || newPosition.y > yMax)
        {
            newPosition.y = Mathf.Clamp(newPosition.y, -yMax, yMax);
        }

        transform.position = newPosition;

    }

    public void setStart()
    {
        start = true;
    }

    public void setStartEnded()
    {
        startEnded = true;
    }

    void CameraShake()
    {
        if (shakeAmt > 0)
        {
            float quakeAmt = Random.value * shakeAmt * 2 - shakeAmt;
            Vector3 pp = mainCamera.transform.position;
            pp.y += quakeAmt;
            pp.x += quakeAmt;
            mainCamera.transform.position = pp;
        }
    }

    void StopShaking()
    {
        CancelInvoke("CameraShake");
        mainCamera.transform.position = new Vector3(0, 0, -10);
    }

    public void Heal()
    {
        health = maxHealth;
    }

    public void AddDmg()
    {
        damage += 10;
    }

    public void AddMaxHealth()
    {
        maxHealth += 30;
    }
}
