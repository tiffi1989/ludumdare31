﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MobSpawner : MonoBehaviour
{

    public GameObject enemy;
    private float time;
    private int waveNumber;
    private ArrayList enemyList;
    public GameObject[] prevabs;
    public Text waveText;
    public Canvas canvas;
    private bool aInvoke;
    private bool gameStarted;
    private int enemyDamage;
    public GameObject player;
    int bombo;



    // Use this for initialization
    void Start()
    {
        waveNumber = 1;
        enemyList = new ArrayList();
        enemyDamage = 5;
        bombo = 0;


    }

    // Update is called once per frame
    void Update()
    {

        GameObject enemyToDelete = null;

        foreach (GameObject enemy in enemyList)
        {
            if (enemy.Equals(null))
            {
                enemyToDelete = enemy;

            }
        }

        enemyList.Remove(enemyToDelete);


        if (enemyList.Count == 0 && waveNumber > 0 && !aInvoke && gameStarted)
        {
            if (waveNumber == 1)
            {
                startNewWave2();
            }
            else {
                canvas.GetComponent<Animator>().SetBool("levelup", true);
                aInvoke = true;
            }

            

                        
        }



        time += Time.deltaTime;

    }

    void startNewWave()
    {
       
            waveText.text = "Wave " + waveNumber;
            Animator ani = canvas.GetComponent<Animator>();
            ani.SetTrigger("wave");
            Invoke("startNewWave2", 3);
            Debug.Log("WaveNumber " + (waveNumber - 1));
        
       

    }

    void startNewWave2()
    {

        switch (waveNumber)
        {


            case 1: spawnEnemy();

                break;

            case 2: spawnEnemy();
                Invoke("spawnEnemy", 0.5f);

                break;

            case 3: spawnEnemy();
                Invoke("spawnEnemy", 0.5f);
                Invoke("spawnEnemy", 1);
                break;



            default:

                break;

        }

        if (waveNumber > 3)
        {
            spawnEnemy();
            float f = 0.5f;

            for (int i = 0; i <= waveNumber; i++)
            {
                f += 0.5f;
                Invoke("spawnEnemy", f);
                f += 0.5f;
            }
        }



        enemyDamage += waveNumber;
        player.SendMessage("SetEnemyDamage", enemyDamage);
        waveNumber++;
        aInvoke = false;
    }

    void spawnEnemy()
    {

        GameObject prefabo = prevabs[0];
        

        if (waveNumber > 3)
        {


            bombo++;
            if (bombo > 3)
            {
                prefabo = prevabs[1];
                bombo = 0;
            }
        }

        GameObject myNewObject = Instantiate(prefabo, transform.position, transform.rotation) as GameObject;
        if(prefabo.Equals(prevabs[0]))
        {
            myNewObject.SendMessage("SetLife", 50 + 4 * waveNumber);
        }
        enemyList.Add(myNewObject);
        Debug.Log("spawnEnemy");

    }

    public void LeveldUp()
    {
        Invoke("startNewWave", 1);

        canvas.GetComponent<Animator>().SetBool("levelup", false);
        
    }

    void nextWave()
    {
        waveNumber++;
    }

    void startNextWave()
    {
        gameStarted = true;
    }

    public void startFirstWave()
    {
        Invoke("startNextWave", 5f);
    }

    void calcWave()
    {

    }
}
