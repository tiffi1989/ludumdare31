﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Bubble : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("kill", 2);
        Text text = gameObject.GetComponentInChildren<Text>();
        text.text = "" + Mathf.RoundToInt(transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void kill()
    {
        Destroy(gameObject);
    }
}
