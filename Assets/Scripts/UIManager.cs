﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public Button startButton; 
    public Button settingsButton;
    public Slider slider;
    public Button settingsCloseButton;
    public Canvas canvas;
    public Slider volume;
    

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void RestartGame()
    {
        Application.LoadLevel("TheOneScene");
    }



    public void ClickedFalse()
    {
        canvas.GetComponent<Animator>().SetBool("clicked", false);
    }

    public void ClickedTrue()
    {
        canvas.GetComponent<Animator>().SetBool("clicked", true);

    }

    public void SettingsTrue()
    {
        canvas.GetComponent<Animator>().SetBool("settings", true);
        Slider slide = volume.GetComponent<Slider>();
        slide.interactable = true;

    }

    public void SettingsFalse()
    {
        Slider slide = slider.GetComponent<Slider>();
        slide.interactable = false;

        canvas.GetComponent<Animator>().SetBool("settings", false);

    }
}
