﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VolumeControl : MonoBehaviour {

   public Camera myCam;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setVolume(){
        AudioSource myAudio =  myCam.GetComponent<AudioSource>();
        Slider slider = gameObject.GetComponent<Slider>();
        myAudio.volume = slider.value;
    }

    public void toggleVolume()
    {
        AudioSource myAudio = myCam.GetComponent<AudioSource>();
        Slider slider = gameObject.GetComponent<Slider>();
        if (myAudio.mute == false)
        {
            myAudio.mute = true;
        }
        else
        {
            myAudio.mute = false;
        }
    }
   
}
