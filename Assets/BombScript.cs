﻿using UnityEngine;
using System.Collections;

public class BombScript : MonoBehaviour {

    private bool visible;
    public int speed;
    private ParticleSystem particle;
    private GameObject spawnPoint;
    private GameObject player;
    public GameObject bubble;
    private GameObject canvas;
    private float time;
    private float timehit;
    private Animator animator;
    private bool dead;
    private int damage;



    // Use this for initialization
    void Start()
    {
        visible = false;
        transform.eulerAngles = new Vector3(0, 0, 180);
        spawnPoint = GameObject.FindGameObjectWithTag("SpawnPoint");
        Transform[] allChildren = spawnPoint.GetComponentsInChildren<Transform>();
        allChildren[1].transform.parent = gameObject.transform;
        particle = gameObject.GetComponentInChildren<ParticleSystem>();
        player = GameObject.FindGameObjectWithTag("PlayerTag");
        canvas = GameObject.FindGameObjectWithTag("Canvas");
        animator = gameObject.GetComponent<Animator>();
        CharController character = player.GetComponent<CharController>();
        damage = character.damage;
        Invoke("explode", Random.Range(5,10));

    }

    public void explode()
    {
        dead = true;
        
        
        Invoke("explode2", 0.1f);
        animator.SetTrigger("explode");
    }

    private void explode2()
    {
        playSound();
        particle.Play();
        Invoke("Kill", 0.1f);
    }

    public void playSound()
    {
        AudioSource sa = gameObject.GetComponent<AudioSource>();
        sa.Play();
    }
    
    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        timehit += Time.deltaTime;

        if (!dead)
        {




            if (visible)
            {

                Vector3 otherVec = player.transform.position;
                Vector3 thisVec = gameObject.transform.position;

                float angle = Mathf.Atan2(otherVec.y - thisVec.y, otherVec.x - thisVec.x) * Mathf.Rad2Deg;




                gameObject.transform.eulerAngles = new Vector3(0, 0, angle);
                transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime * speed);
            }
            else
            {

                transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime * speed);
                transform.eulerAngles = new Vector3(0, 0, 90);
            }
        }
    }



    void Kill()
    {
        player.SendMessage("shakeIt");
        Destroy(gameObject);
    }


    void OnBecameVisible()
    {
        Invoke("setVisibleTrue", 1f);
    }

    void setVisibleTrue()
    {
        visible = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
      

    }
}
